
  !-*-mode: f90;mode: font-lock;-*-

      module variables

      implicit none

        integer :: Nring,iseed,Natom,Npolymer,Nsite,Neigenstate
        integer :: Ninterval,Npolymer_t
        integer :: bz,volume

        double precision :: bxy
        double precision :: pi,sig_phi,phi_init, sig_alpha
        double precision :: gasdev
        double precision :: ran1
        double precision :: out_box
        double precision :: Pcis_trans
        double precision :: dot,F,D_E,A_E
        integer:: Ngrid

        double precision, dimension(:,:), allocatable :: x,y,z
        double precision, dimension(:,:), allocatable :: phi
        double precision, dimension(:,:,:), allocatable :: n_cg

!      Finding legs variables
        integer :: eigenstate, icount,max_nlegs
        integer,dimension(:),allocatable::nlegs

       !integer, dimension(Npolymer) :: nlegs

!      Diagonalising Hamiltonian variables
        double precision, dimension(:,:), allocatable :: psi
        double precision, dimension(:,:,:), allocatable :: psi_legs_d
        double precision,dimension(:,:),allocatable::E_legs_d

!      Memory saving
       double precision, dimension(:,:),allocatable :: E_legs
       double precision, dimension(:,:,:),allocatable :: psi_legs
       double precision::sigma

!      Geometry Parameters

!      Declaring fundamental constants
       double precision :: hbar,T,kb,field

!      radius gyration
       double precision,dimension(:),allocatable::rad_gyr
       double precision::ave_rad_gyr
       double precision, dimension(:,:),allocatable::CofM_polymer

!      site coordinates
       double precision, dimension(:,:), allocatable::x_site,y_site,z_site
       double precision, dimension(:,:), allocatable::leg_loc
       double precision::ave_leg_loc
       integer, dimension(:),allocatable ::c_s
       double precision, dimension(:,:,:),allocatable::prob_e_pz
       double precision,dimension(:,:,:),allocatable::CofM

!     twin polymer

      double precision,dimension(:,:),allocatable::x_t,y_t,z_t
      double precision,dimension(:,:,:),allocatable::n_cg_t
      double precision,dimension(:,:),allocatable :: E_legs_t
      double precision,dimension(:,:,:),allocatable::psi_legs_t
      integer,dimension(:),allocatable::nlegs_t
      double precision,dimension(:,:,:),allocatable::CofM_t
      double precision, dimension(:,:,:),allocatable::prob_e_pz_t
      double precision,dimension(:,:),allocatable::CofM_polymer_t

! overlap

      integer,dimension(:,:,:,:),allocatable::n_map
      integer,dimension(:,:),allocatable::ny

!      marcus theory variables
       double precision,dimension(:,:,:,:),allocatable::W_da
       double precision,dimension(:,:),allocatable::E_lambda
       double precision,dimension(:,:,:,:),allocatable::k_da
       integer,dimension(:),allocatable::flag_t

!     monte carlo
       double precision,dimension(:),allocatable::time,distance, &
            rmsdistance
       double precision,dimension(:,:),allocatable::drift_velocity,&
            mobility
       integer,dimension(:,:),allocatable::hop_number

!     polymer length
       double precision,dimension(:),allocatable::long
             
!cpu

    end module variables


!************************************************************************

      program PPV_geom

        use variables 

        implicit none

        integer :: i,j,k,py,m
        
!      Finding legs variables
!      integer :: eigenstate, icount,max_nlegs
!      integer, dimension(Npolymer) :: nlegs
!      Diagonalising Hamiltonian variables
!      Memory saving
!      Geometry Parameters
!      Declaring fundamental constants
!      radius gyration
!      site coordinates
!     twin polymer
!     overlap
!      marcus theory variables
!     monte carlo       

!cpu
       double precision::tstart,t1,t2,t3,t4,tstop
       integer::d

       call cpu_time(tstart)

!     Fundamental constants
      !kb = 1.38066503d-23 ! J/K

!     Model Parameters
      
      call read_parameters
!(Nring,Npolymer,sig_phi,iseed,phi_init, &
 !          & Pcis_trans,Ngrid,out_box,Ninterval,T,field,sig_alpha,T

      print*,'Nring,Npolymer,sig_phi,iseed,phi_init'
      print*,Nring,Npolymer,sig_phi,iseed,phi_init
      print*,'Pcis_trans,Ngrid,out_box,Ninterval,T'
      print*,Pcis_trans,Ngrid,out_box,Ninterval,T
      print*,'field,sig_alpha,T',field,sig_alpha,T

!     array parameters
      Natom = 8*Nring - 2
      Nsite = 2*Nring - 1
      Neigenstate = Nsite
      Npolymer_t=7*Npolymer

      pi = 4.0d0*datan(1.0d0)      
      hbar = 1.05457160d-34 !Js
      kb= 8.617332478d-5 !eV/K
      
      Natom = 8*Nring - 2
      Nsite = 2*Nring - 1
      Neigenstate = Nsite
      Npolymer_t=7*Npolymer

!     allocate geometry arrays
      allocate(X(Natom,Npolymer),Y(Natom,Npolymer),Z(Natom,Npolymer))
      allocate(phi((9*Nring-3),Npolymer))
      allocate(psi(Nsite,Nsite))
      allocate(rad_gyr(Npolymer))
      allocate(CofM_polymer(3,Npolymer))
      allocate(n_cg(3,Nsite,Npolymer))
      allocate(x_site(Nsite,Npolymer))
      allocate(y_site(Nsite,Npolymer))
      allocate(z_site(Nsite,Npolymer))
      allocate(psi_legs_d(Nsite,Neigenstate,Npolymer))
      allocate(E_legs_d(Neigenstate,Npolymer))
      allocate(nlegs(Npolymer))
      allocate(long(Npolymer))

!cpu t1
      call cpu_time(t1)

      call box!(Npolymer,Nring,bxy,bz,pi)
      print*,'box'

      call assign_angles!(Nring,Npolymer,iseed,sig_phi,phi_init,phi)
      print*,'1'

      phi=phi*pi/180.d0

      call calculate_geometry!(Nring,iseed,Natom,Npolymer,phi,sig_phi, &
       ! & phi_init,x,y,z,rring,slink,dlink,theta,alpha,eta,l,pi,neta, &
        !& Nsite,n_cg,bxy,bz,Pcis_trans,CofM_polymer,ave_rad_gyr,rad_gyr)
      print*,'2'

!      call radius_gyration(x,y,z,Npolymer,Natom,rad_gyr,ave_rad_gyr,CofM_polymer)


      call site_density!(bz,Ngrid,Npolymer,Natom,z,out_box)

!cpu t2
      call cpu_time(t2)

      call find_legs!(Npolymer,Nring,Nsite,E_0,phi,Neigenstate,psi_legs_d,E_legs_d,nlegs,max_nlegs,sig_alpha)
      print*,'3'

      allocate(E_legs(max_nlegs,Npolymer))
      allocate(psi_legs(Nsite,max_nlegs,Npolymer))

      call memory_save!(Nsite,Neigenstate,Npolymer,max_nlegs,psi_legs_d,E_legs_d,psi_legs,E_legs,nlegs,sig_phi,phi_init,sigma,pi)

      allocate(leg_loc(Neigenstate,Npolymer))
      allocate(c_s(Natom))
      allocate(prob_e_pz(Natom,Neigenstate,Npolymer))
      allocate(W_da(Neigenstate,Neigenstate,Npolymer,Npolymer_t))
      allocate(E_lambda(Neigenstate,Npolymer_t))
      allocate(x_t(Natom,Npolymer_t),y_t(Natom,Npolymer_t), &
           z_t(Natom,Npolymer_t))
      allocate(CofM(3,Neigenstate,Npolymer))
      allocate(E_legs_t(Neigenstate,Npolymer_t))
      allocate(psi_legs_t(Nsite,Neigenstate,Npolymer_t))
      allocate(nlegs_t(Npolymer_t))
      allocate(CofM_t(3,Neigenstate,Npolymer_t))
      allocate(n_cg_t(3,Nsite,Npolymer_t))
      allocate(prob_e_pz_t(Natom,Neigenstate,Npolymer_t))
      allocate(CofM_polymer_t(3,Npolymer_t))

      allocate(time(1000000))
      allocate(distance(1000000))
      allocate(rmsdistance(1000000))
      allocate(drift_velocity(Npolymer,Neigenstate))
      allocate(hop_number(Npolymer,Neigenstate))
      allocate(k_da(Neigenstate,Neigenstate,Npolymer,Npolymer_t))
      allocate(flag_t(Npolymer_t))
      allocate(mobility(Npolymer,Neigenstate))

      allocate(n_map(Npolymer,Neigenstate,5*Npolymer_t,2))
      allocate(ny(Npolymer,Neigenstate))


      call coordinate_site!(Nring,Npolymer,Nsite,Natom, &
           ! & x,y,z,x_site,y_site,z_site)
    
      call atom_allocated_to_site!(Natom,c_s)

      call average_electron_position!(Natom,Nsite,Neigenstate,Nring, &
          ! & Npolymer,psi_legs,nlegs,x_site,y_site,z_site,CofM)

      call leg_localised_length!(Nsite,Npolymer,nlegs,psi_legs,leg_loc,Neigenstate,ave_leg_loc)
      print*,'4'

     call pz_carbon!(Nring,Natom,Nsite,Neigenstate,Npolymer,psi_legs, &
           !& nlegs,c_s,prob_e_pz)
     print*,'pz'

!cpu t3
     call cpu_time(t3)

     call twin_polymer!(Natom,Npolymer,x,y,z,n_cg,E_legs,psi_legs, &
       ! & nlegs,CofM,prob_e_pz,Npolymer_t,x_t,y_t,z_t,n_cg_t,E_legs_t, &
        !& psi_legs_t,nlegs_t,CofM_t,prob_e_pz_t,bz,bxy,Nsite,Neigenstate,flag_t,CofM_polymer,CofM_polymer_t)

     print*,'5'


     call overlap!(Npolymer_t,Nsite,Natom,Neigenstate,n_cg_t,pi,x_t,&
         !& y_t,z_t,c_s,prob_e_pz_t,nlegs_t,W_da,CofM_polymer_t,ave_rad_gyr,n_map,ny,CofM_t)

     print*,'6'
     
     call marcus_charge_transfer!(Nsite,Natom,Neigenstate,Npolymer_t, &
       ! & psi_legs_t,E_legs_t,nlegs_t,W_da,Ninterval,hbar,kb,T,pi, &
       ! & E_lambda,CofM_t,field,k_da,n_map,ny)    
      print*,'7'
!cpu t4


      call cpu_time(t4)

      call monte_carlo!(Npolymer_t,nlegs_t,k_da,time,intra_hop,hop_number,Neigenstate,CofM_t,flag_t,bz,bxy,iseed,distance,rmsdistance,drift_velocity,field,mobility,n_map,ny,CofM_polymer_t)

      print*,'monte carlo'

!cpu t stop
      call cpu_time(tstop)

!RESULTS
      open(unit=9,file="results",status='unknown')
      write(9,*)
      write(9,*)'Npolymer',Npolymer
      write(9,*)'Nring',Nring
      write(9,*)'field (V/A)',field
      write(9,*)'Pcis_trans',Pcis_trans
      write(9,*)'Temperature',T
      write(9,*)'sig_phi',sig_phi
      write(9,*)'phi_init',phi_init
      write(9,*)'sigma eV',sigma
      write(9,*)
      write(9,*) 'Average radius gyration (A)',ave_rad_gyr
      write(9,*) 'Average length of LEG (in carbon atoms)',ave_leg_loc
      write(9,*)
      do py=1,Npolymer

         write(9,*)
         write(9,*)'Radius of gyration (A)',rad_gyr(py)
         write(9,*)
         do i=1,nlegs(py)
            write(9,*)'Polymer,leg', py,i
            write(9,*)'Drift velocity (A/s)',drift_velocity(py,i)
            write(9,*)'Mobility (A**2/Vs)', mobility(py,i)
            write(9,*)            
         end do
      end do
      write(9,*),'cpu_time (s)',tstop-tstart

      close(9)

      end program PPV_geom

!*************************************************************************

      subroutine box
        
        use variables, only:Npolymer,bxy,bz,volume

        implicit none

        bxy = dsqrt(volume/dble(bz))

!4 * dsqrt(dble(Npolymer))

        print*,'bz,bxy',bz,bxy

      end subroutine box


!*************************************************************************

! makes an array of normally distributed angles called phi 

      subroutine assign_angles

        use variables, only:Nring,Npolymer,iseed,sig_phi,phi_init,phi
        
        implicit none

!     Initialise Randomly Distributed Values of phi
        
        integer :: k,py,i 

        double precision :: gasdev
        double precision :: ran1


!    100 polymers
        do py=1,Npolymer

           do k=1, 9*Nring-3
              
              phi(k,py)=0.d0
              
           end do
           
           do i=0,Nring-2

              phi(7+9*i,py)= gasdev(iseed,sig_phi,phi_init)
              phi(8+9*i,py)= gasdev(iseed,sig_phi,phi_init)     
              phi(9+9*i,py)= gasdev(iseed,sig_phi,phi_init)
           end do
           
           
        end do
        
      end subroutine assign_angles

!***********************************************************************
      subroutine read_parameters

        use variables

        implicit none

        open(unit=8,file='parameters',status='unknown')

        read(8,*) ! Nring
        read(8,*) Nring
        read(8,*) ! Npolymer
        read(8,*) Npolymer
        read(8,*) ! sig_phi
        read(8,*) sig_phi
        read(8,*) ! sig_alpha
        read(8,*) sig_alpha
        read(8,*) ! phi_init
        read(8,*) phi_init
        read(8,*) ! iseed
        read(8,*) iseed
        read(8,*) !Pcis_trans
        read(8,*) Pcis_trans
        read(8,*) !Ngrid
        read(8,*) Ngrid
        read(8,*) !out_box
        read(8,*) out_box
        read(8,*) !Ninterval
        read(8,*) Ninterval
        read(8,*) !field
        read(8,*) field
        read(8,*) !temperature
        read(8,*) T
        read(8,*) !bz
        read(8,*) bz
        read(8,*) !volume
        read(8,*) volume

        close(unit=8)

      end subroutine read_parameters

!*****************************************************************************
! assigns the coordinates of each atom of each polymer
   subroutine calculate_geometry
        
     use variables,only:Nring,iseed,Natom,Npolymer,phi,sig_phi,phi_init, &
          x,y,z,pi,Nsite,n_cg,bxy,bz,Pcis_trans,CofM_polymer,ave_rad_gyr,&
          rad_gyr,hbar,long

     implicit none

      integer :: i,k,py,m,cg,a,j,g,s

      double precision :: gasdev
      double precision :: ran1

      double precision, dimension(Natom,Npolymer):: xnew,ynew,znew
      double precision, dimension(3,Nsite,Npolymer):: n_cgnew,n_cg2

      double precision, dimension(3,Npolymer)::new_cofm
      double precision,dimension(3)::r

!     Aligning polymer
      double precision::angle
      double precision,dimension(3)::axis,z_axis
      !double precision,dimension(3)::r
      double precision,dimension(3)::vector,vector2
      double precision::dot,cross
      double precision::norm

!     Geometry Parameters

      double precision, dimension(3) :: v1,v2,v3,n,p,r1
      double precision :: rring,slink,dlink
      double precision :: theta       ! 60 deg
      double precision :: alpha       ! 128 deg
      double precision :: eta,neta    ! 53 deg
      double precision :: l           ! distance across ring

!     Declaring fundamental constants

      double precision::length

      double precision::sep
      integer::count

      theta = 60.0d0/180.0d0*pi
      alpha = 120.0d0/180.0d0*pi
      eta = 60.0d0/180.0d0*pi

      rring = 1.39d0
      slink = 1.44d0
      dlink = 1.33d0

      l=rring*(1.0d0+2.0d0*dcos(theta))

      angle=0.1797
      z_axis(1)=0.d0
      z_axis(2)=0.d0
      z_axis(3)=1.d0
     
      open(unit=10,file='coordinates1.txt',status='unknown')
      write(10,*) 'Coordinates'
      close(10)

      do py=1,Npolymer

!     First Ring

      x(1,py)= ran1(iseed)*bxy
      y(1,py)= ran1(iseed)*bxy
      z(1,py)= ran1(iseed)*bz

      x(2,py)=x(1,py)
      y(2,py)=y(1,py)-rring*dsin(theta)
      z(2,py)=z(1,py)+rring*dcos(theta)

      x(3,py)=x(1,py)
      y(3,py)=y(2,py)
      z(3,py)=z(1,py)+rring*(1+dcos(theta))

      x(4,py)=x(1,py)
      y(4,py)=y(1,py)
      z(4,py)=z(1,py)+rring*(1+2*dcos(theta))

      x(5,py)=x(1,py)
      y(5,py)=y(1,py)+rring*dsin(theta)
      z(5,py)=z(3,py)

      x(6,py)=x(1,py)
      y(6,py)=y(5,py)
      z(6,py)=z(2,py)

      cg=0
 
!     Remaining Rings

          do i=0,(Nring-2)

!     Atom 7,15 etc.
         ! v1 = vector with origin at atom 4 to atom 7
             v1(1) = (x(4+i*8,py)-x(1+i*8,py))*(slink/l)
             v1(2) = (y(4+i*8,py)-y(1+i*8,py))*(slink/l)
             v1(3) = (z(4+i*8,py)-z(1+i*8,py))*(slink/l)

             x(7+i*8,py)= v1(1) + x(4+i*8,py)
             y(7+i*8,py)= v1(2) + y(4+i*8,py)
             z(7+i*8,py)= v1(3) + z(4+i*8,py)

         !Atom 8,16 etc.

             v2(1) = x(7+i*8,py)-x(3+i*8,py)
             v2(2) = y(7+i*8,py)-y(3+i*8,py)
             v2(3) = z(7+i*8,py)-z(3+i*8,py)

             call cross(v1,v2,n)

             cg=cg+1
             n_cg(:,cg,py)=n(:)

             p = v1*(dlink/slink)

             call cis_trans_defect(eta,neta)
 

                call rotate(p,n,neta,r1)
                
                call rotate(r1,v1,phi(7+9*i,py),v3)
                
                x(8+i*8,py)= v3(1) + x(7+i*8,py)
                y(8+i*8,py)= v3(2) + y(7+i*8,py)
                z(8+i*8,py)= v3(3) + z(7+i*8,py)

                sep=dsqrt((x(8+i*8,py)-x(1,py))**2 +(y(8+8*i,py)-y(1,py))&
                     **2)
                    
             !atom 9,17 etc.
             
             v1(1) = x(8+i*8,py)-x(4+i*8,py)
             v1(2) = y(8+i*8,py)-y(4+i*8,py)
             v1(3) = z(8+i*8,py)-z(4+i*8,py)
             
             call cross(v3,v1,n)
          
             cg=cg+1
             n_cg(:,cg,py)=-n(:) ! NB -ve sign

             p = v3*(slink/dlink)
             
             call rotate(p,n,eta,r1)
             
             call rotate(r1,v3,phi(8+i*9,py),v2)
             
             x(9+i*8,py)= v2(1) + x(8+i*8,py)
             y(9+i*8,py)= v2(2) + y(8+i*8,py)
             z(9+i*8,py)= v2(3) + z(8+i*8,py)
             
! calculating angle of rotation to align the polymers in the z direction
             !angle=datan((y(9,py)-y(1,py))/(z(9,py)-z(1,py)))
             
             !print*,'angle',angle

             !atom 10,18 etc.
             
             v1(1) = x(9+i*8,py)-x(7+i*8,py)
             v1(2) = y(9+i*8,py)-y(7+i*8,py)
             v1(3) = z(9+i*8,py)-z(7+i*8,py)


             call cross(v1,v2,n)
             
             p = v2*(rring/slink)
             
             call rotate(p,n,theta,r1)
             
             call rotate(r1,v2,phi(9+9*i,py),v3)
             
             x(10+i*8,py)= v3(1) + x(9+i*8,py)
             y(10+i*8,py)= v3(2) + y(9+i*8,py)
             z(10+i*8,py)= v3(3) + z(9+i*8,py)
             
             !atoms 11,19 etc.
             
             x(11+i*8,py)= p(1) + x(10+i*8,py)
             y(11+i*8,py)= p(2) + y(10+i*8,py)
             z(11+i*8,py)= p(3) + z(10+i*8,py)
             
             !atoms 14,22 etc.
             
             call rotate(p,n,-theta,r1)
             
             call rotate(r1,v2,phi(9+9*i,py),v1)
             
             x(14+i*8,py)= v1(1) + x(9+i*8,py)
             y(14+i*8,py)= v1(2) + y(9+i*8,py)
             z(14+i*8,py)= v1(3) + z(9+i*8,py)
             
             !atoms 13,21 etc.
             
             x(13+i*8,py)= p(1) + x(14+i*8,py)
             y(13+i*8,py)= p(2) + y(14+i*8,py)
             z(13+i*8,py)= p(3) + z(14+i*8,py)
             
             !atoms 12,20 etc.
             
             x(12+i*8,py)= v1(1) + x(11+i*8,py)
             y(12+i*8,py)= v1(2) + y(11+i*8,py)
             z(12+i*8,py)= v1(3) + z(11+i*8,py)

          end do

             i=Nring-1
             v1(1) = (x(4+i*8,py)-x(1+i*8,py))*(slink/l)
             v1(2) = (y(4+i*8,py)-y(1+i*8,py))*(slink/l)
             v1(3) = (z(4+i*8,py)-z(1+i*8,py))*(slink/l)

             v2(1) = x(5+i*8,py)-x(2+i*8,py)
             v2(2) = y(5+i*8,py)-y(2+i*8,py)
             v2(3) = z(5+i*8,py)-z(2+i*8,py)

             call cross(v1,v2,n)

             cg=Nsite
             n_cg(:,cg,py)=n(:)

!     box-periodic boundary conditions, b is the limit of the box
!     m is a dummy variable             

      open(unit=10,file='coordinates1.txt',status='unknown',position=&
           'append')
               do i = 1,Natom
                  write(10,100)i, x(i,py),y(i,py),z(i,py)
100               format(i3,e14.5,e14.5,e14.5)
               end do

               do k=7,9 * (Nring - 1),9
                  write (10,101)k, phi(k,py)*180.0d0/pi
101               format(i10,e14.5)
               end do

      close(10)

      
      r(1)=x(Natom-2,py)-x(1,py)
      r(2)=y(Natom-2,py)-y(1,py)
      r(3)=z(Natom-2,py)-z(1,py)

      norm=dsqrt(r(1)**2+r(2)**2+r(3)**2)

      r(1)=r(1)/norm
      r(2)=r(2)/norm
      r(3)=r(3)/norm

      angle=acos(dot(z_axis,r))

      print*, 'angle = ', angle*57

      call cross(z_axis,r,axis)

      print*, 'axis = ', axis

      axis=axis/dsqrt(dot(axis,axis))

      do i=2,Natom
         vector(1)=x(i,py)-x(1,py)
         vector(2)=y(i,py)-y(1,py)
         vector(3)=z(i,py)-z(1,py)

         call rotate(vector(:),axis,-angle,vector2(:))

         x(i,py)=vector2(1)+x(1,py)
         y(i,py)=vector2(2)+y(1,py)
         z(i,py)=vector2(3)+z(1,py)

      end do

      do cg=1,Nsite

         call rotate(n_cg(:,cg,py),axis,-angle,n_cg2(:,cg,py))

      end do

   end do ! 100 polymers (py) done
  
   do py = 1, Npolymer
      do cg = 1, Nsite
         n_cg(:,cg,py) = n_cg2(:,cg,py)/dsqrt(sum(n_cg2(:,cg,py)**2))
      end do
   end do

   do py=1,Npolymer
      long(py)=z(Natom-2,py)-z(1,py)
   end do
   length = sum(long(:))/Npolymer
   print*,'length',length
   
   
   call radius_gyration

   
   j=Npolymer**(1.d0/2.d0)
   k=0.d0
   g=0.d0

   do py=1,Npolymer
      g=g+1.d0
      if (mod(py,j).eq.1.d0)then
         k=k+1.d0
         g=1.d0
         print*,py,k,j,bxy
      end if
      new_cofm(1,py)=dble(g)*bxy/dble(j)
      new_cofm(2,py)=dble(k)*bxy/dble(j)
      new_cofm(3,py)=dble(bz)/2.d0 + (ran1(iseed)-0.5d0)*dble(bz)/2.d0

      do i=1,3
         r(i)=new_cofm(i,py)-CofM_polymer(i,py)
         CofM_polymer(i,py)=new_cofm(i,py)
      end do
      
      do a=1,Natom
         x(a,py)=x(a,py)+r(1)
         y(a,py)=y(a,py)+r(2)
         z(a,py)=z(a,py)+r(3)
      end do
   end do
   
   do py=1,Npolymer
      write(6,300)py,rad_gyr(py),CofM_polymer(1,py),CofM_polymer(2,py),&
           CofM_polymer(3,py)
   end do
   
300 format(i3,4f8.2)



!    call boundary_conditions(Npolymer,Natom,Nsite,z,n_cg)

   open(unit=10,file='coordinates2.txt',status='unknown')
   write(10,*)Natom*Npolymer
   write(10,*)
   do py=1,Npolymer
      do j = 1,Natom
         write(10,102)py, x(j,py),y(j,py),z(j,py)
102      format(i4,e14.5,e14.5,e14.5)
      end do

   end do
 
   close(10)

 end subroutine calculate_geometry

!***********************************************************************

 !subroutine boundary_conditions(Npolymer,Natom,Nsite,z,n_cg)
   
  ! implicit none
   
   !integer::Npolymer,Natom,Nsite
!   integer::py,a,cg
!   double precision, dimension(Natom,Npolymer):: z
 !  double precision,dimension(3,Nsite,Npolymer)::n_cg

  ! do py=1,Npolymer,2
   !   do a=2,Natom
    !     z(a,py)=-(z(a,py)-z(1,py))+z(1,py)
     !end do
  ! end do

! end subroutine boundary_conditions

!**********************************************************************

 subroutine isotropy(xnew,ynew,znew,n_cgnew)

   use variables,only:Natom,Npolymer,Nsite,x,y,z,n_cg

   implicit none
   
   integer::py,a,s
   double precision,dimension(Natom,Npolymer)::xnew,ynew,znew
   double precision, dimension(3,Nsite,Npolymer)::n_cgnew


   do py=1,Npolymer
      do a=1,Natom
         xnew(a,py)=z(a,py)
         ynew(a,py)=y(a,py)
         znew(a,py)=x(a,py)
      end do
      do s=1,Nsite
         n_cgnew(1,s,py)=n_cg(3,s,py)
         n_cgnew(2,s,py)=n_cg(2,s,py)
         n_cgnew(3,s,py)=n_cg(1,s,py)
      end do
   end do

 end subroutine isotropy
 

!***********************************************************************
! find the localised electron ground states, by diagonalising the huckel hamiltonian to find the eigenvalues and eigenfunctions of the diagonalised hamiltonian. 

! currently have an array of angles phi for (Natoms,Npolymer)
! want to construct E and D for each polymer

     subroutine find_legs

       use variables,only:Npolymer,Nring,Nsite,phi,Neigenstate,&
            psi_legs_d,E_legs_d,nlegs,max_nlegs,sig_alpha,psi,iseed

       implicit none

       integer :: py,k,m,i
       integer :: eigenstate, icount,s,q,n,j

       double precision :: E_0, beta, E_min, gasdev
       double precision, dimension(Nsite) :: D
       double precision, dimension(Nsite-1) :: E

       psi_legs_d=0.d0
       E_legs_d=0.d0
       E_0 = 9.237d0
             
       
       do py = 1, Npolymer

!        Construct the tri-diagonal matrix

               D = 0.0d0
               E = 0.0d0

               m=1

!***************
               do k = 0, Nring-2
                  
                  E(m)=-2.2d0*(dcos(phi(7+9*k,py)))/dsqrt(6.0d0) !t0=2.2eV
                  E(m+1)=-2.2d0*(dcos(phi(9+9*k,py)))/dsqrt(6.0d0)
                  D(m) = 4.8d0 + gasdev(iseed,sig_alpha,0.0d0)
                  D(m+1) = 3.2d0*(dcos(phi(8+9*k,py))) + gasdev(iseed,&
                       sig_alpha,0.0d0)
                  
                  m=m+2
                  
               end do
           
               D(Nsite) = 4.8d0
 
               call diagonalise_Ham(D,E)

!now D gives the eigenvalues for each eigenstate of the polymer and psi gives the eigenfunctions for the polymer.

! want to calculate the legs for each polymer
                          
               icount = 0

               do eigenstate = 1, Neigenstate
                  
                  beta = sum(abs(psi(:,eigenstate))*psi(:,eigenstate))
                  
                  if (abs(beta) >= 0.95) then  ! localised wfn and energy
                     
                                                                 
                         icount = icount + 1
                         psi_legs_d(:,icount,py) = psi(:,eigenstate)
                         E_legs_d(icount,py) = D(eigenstate)
                         
                                               
                            if (E_legs_d(icount,py)<E_min) then
                                 E_min = E_legs_d(icount,py)
                            end if
                     
                  end if
                  
               end do ! for each eigenstate
               
! number of legs of each polymer

                nlegs(py) = icount

             end do ! each polymer done...

             max_nlegs=maxval(nlegs)


             open(unit=10,file='eigenstate.txt',status='unknown')!,position='append')

               do i = 1,Npolymer
                   do q=1,nlegs(i)
                      write(10,*) 'Eigenvalue'
                      write(10,200) nlegs(i), E_legs_d(q,i)
                      write(10,*) 'Eigenstate'
                     do s=1,Nsite
                  write(10,200)s, psi_legs_d(s,q,i)
200               format(i3,f14.5)
                     end do
                  write(10,*)
                  end do
               end do

      close(10)

    end subroutine find_legs


!***********************************************************************

! diagonlises a tridiagonal matrix for each polymer
       
        subroutine diagonalise_Ham(D,E)

          use variables,only:Nsite,Neigenstate,psi

        implicit none

        integer :: Nwork, info
        double precision, dimension(Nsite) :: D
        double precision, dimension(Nsite-1) :: E

        ! temporary array
        double precision, dimension(:), allocatable :: work

        Nwork = 2*(Nsite-1)
        info = 0

        allocate(work(Nwork))

!       For LAPACK:
!       diagonalize a real, symmetrix tridiagonal matrix
!       D contains the N diagonal elements
!       E contains the (N-1) off-diagonal elements
!       The eigenvectors are returned as the columns of Z, eigenvalues in D

        call dstev('v',Nsite,D,E,psi,Nsite,work,info)

        deallocate(work)

        end subroutine diagonalise_Ham

!***********************************************************************

   subroutine memory_save

     use variables,only:Nsite,Neigenstate,Npolymer,max_nlegs,psi_legs_d, &
          E_legs_d,psi_legs,E_legs,nlegs,sig_phi,phi_init,sigma,pi

     implicit none
     
     integer::py,e,s,leg
     double precision :: t0,sigma_e
     double precision::E_legs_total,E_legs_2
     integer::v,u
     double precision::mean,mean_2,square_mean   
          
     psi_legs=0.d0
     E_legs=0.d0

     do py=1,Npolymer
        do e=1,max_nlegs
           do s=1,Nsite
              psi_legs(s,e,py)=psi_legs_d(s,e,py)
           end do
           E_legs(e,py)=E_legs_d(e,py)
        end do
     end do
     
     Neigenstate=max_nlegs

!Finding energetic disorcer/standard deviation

        E_legs_total=0.d0
        E_legs_2=0.d0
        v=0
        u=0

        do py=1,Npolymer
           do leg=1,nlegs(py)
              E_legs_total=E_legs(leg,py)+E_legs_total
              v=v+1
           end do
        end do

        mean=E_legs_total/dble(v)

        mean_2=mean**2

        do py=1,Npolymer
           do leg=1,nlegs(py)
              E_legs_2=E_legs(leg,py)**2+E_legs_2
              u=u+1
           end do
        end do

        square_mean=E_legs_2/dble(u)

        sigma_e=dsqrt(square_mean-mean_2)

        print*,'sigma_e',sigma_e

        open(unit=5,file="energetic_disorder",status='unknown')
        write(5,*)sigma_e
        close(5)        

        

     open( unit=40,file="LEG_energy",status='unknown')
     do py=1,Npolymer
        do leg=1,nlegs(py)
           write(40,*)E_legs(leg,py)
        end do
        write(40,*)
     end do
     close(40)
     
 !standard deviation in the energy of legs - sigma
 
     t0 =2.2d0 !eV

     sigma= t0*(sin((pi/180.d0)*phi_init)*(pi/180.d0)*sig_phi)**&
          (4.d0/3.d0)

   end subroutine memory_save

!************************************************************************

        subroutine cross(a,b,c)

! c = a x b

        implicit none

        double precision, dimension(3) ::a,b,c

        c(1)= a(2)*b(3) - b(2)*a(3)
        c(2)= b(1)*a(3) - a(1)*b(3)
        c(3)= a(1)*b(2) - b(1)*a(2)

        end subroutine cross

!************************************************************************

        function dot(a,b)

        implicit none

        double precision, dimension(3) ::a,b
        double precision::dot

        dot = sum(a(:)*b(:))

        end function dot

!***********************************************************************

       subroutine rotate(a,n,phi,b)

!      Rotate the vector a clockwise by phi radians about the
!      unit vector n to give b.

!      See http://en.wikipedia.org/wiki/Rotation_matrix

       implicit none

       double precision, dimension(3) :: a,b,n
       double precision :: phi,norm,temp1,temp2,temp3

       ! Normalise rotation axis
       norm = dsqrt( n(1)**2 + n(2)**2 + n(3)**2 )
       n = n/norm

       ! Rotation - x coord
       temp1 = dcos(phi) + n(1)**2*(1.0d0-dcos(phi))
       temp2 = n(1)*n(2)*(1.0d0-dcos(phi)) - n(3)*dsin(phi)
       temp3 = n(1)*n(3)*(1.0d0-dcos(phi)) + n(2)*dsin(phi)

       b(1) = temp1*a(1) + temp2*a(2) + temp3*a(3)

       ! Rotation - y coord
       temp1 = n(2)*n(1)*(1-dcos(phi)) + n(3)*dsin(phi)
       temp2 = dcos(phi) + n(2)**2*(1.0d0-dcos(phi))
       temp3 = n(2)*n(3)*(1.0d0-dcos(phi)) - n(1)*dsin(phi)

       b(2) = temp1*a(1) + temp2*a(2) + temp3*a(3)

       ! Rotation - z coord
       temp1 = n(3)*n(1)*(1.0d0-dcos(phi)) - n(2)*dsin(phi)
       temp2 = n(3)*n(2)*(1.0d0-dcos(phi)) + n(1)*dsin(phi)
       temp3 = dcos(phi) + n(3)**2*(1.0d0-dcos(phi))

       b(3) = temp1*a(1) + temp2*a(2) + temp3*a(3)

       end subroutine rotate

!**********************************************************************

       subroutine cis_trans_defect(eta,neta)

         use variables,only:Pcis_trans,iseed

         implicit none
         
         double precision::eta,neta
         double precision :: ran1, x

         neta = eta         
         x = ran1(iseed)

         if(x<=Pcis_trans) then
            neta=-eta
         end if

       end subroutine cis_trans_defect

!*********************************************************************
!average position away from centre of the polymer

       subroutine radius_gyration
         
         use variables,only:Npolymer,Natom,x,y,z,rad_gyr,ave_rad_gyr,&
              CofM_polymer

         implicit none

         integer :: py, i
         double precision :: x_mean,y_mean,z_mean,dot
         double precision,dimension(3)::r
         double precision::tp
         double precision::r2
         double precision::r_2(Npolymer)        
         
         do py=1,Npolymer

            r=0.d0
            r_2=0.d0
            r2=0.d0
            tp=0.d0

            x_mean=sum(x(:,py))/dble(Natom)
            y_mean=sum(y(:,py))/dble(Natom)
            z_mean=sum(z(:,py))/dble(Natom)

            CofM_polymer(1,py)=x_mean
            CofM_polymer(2,py)=y_mean
            CofM_polymer(3,py)=z_mean

            r_2(py)=(dot(CofM_polymer(:,py),CofM_polymer(:,py)))

            do i=1,Natom
               r(1)=x(i,py)
               r(2)=y(i,py)
               r(3)=z(i,py)

               tp = tp + dot(r(:),r(:))
            end do

            r2=tp/dble(Natom)

            rad_gyr(py)=dsqrt(r2-r_2(py))/6.d0

            ave_rad_gyr=sum(rad_gyr(:))/dble(Npolymer)

         end do

         
         do py=1,Npolymer
               write(6,300)py,rad_gyr(py),CofM_polymer(1,py),&
                    CofM_polymer(2,py),CofM_polymer(3,py)
         end do

300     format(i3,4f8.2)
 
                 
       end subroutine radius_gyration

!***********************************************************************

!monomer/carbon density wrt x axis
       
       subroutine site_density

         use variables,only:bz,Ngrid,Npolymer,Natom,out_box,x

         implicit none
         
         integer :: i,g,py,c
         double precision, dimension(Ngrid):: no_carbons,cd
         double precision, dimension(Ngrid):: density
         double precision :: x_grid

         x_grid=(bz+2*out_box)/Ngrid

         no_carbons=0.d0
         density=0.d0

         do py=1,Npolymer
            
             do i=1,Natom

                if (x(i,py).ge.-out_box.and.x(i,py).le.bz+out_box) then
                   g = int((x(i,py)+out_box)/x_grid) + 1
                   no_carbons(g)=no_carbons(g)+1 
                end if
          
             end do
   
          end do

          do c=2,Ngrid
             cd(1)=-out_box+(x_grid/2)
             cd(c)=cd(1)+(c-1)*x_grid
          end do

          density=no_carbons/x_grid

                   
         open(unit=8,file='density',status='unknown')
         do g=1,Ngrid
            write(8,*) cd(g),density(g)
         end do

         close(8)

          end subroutine site_density

!********************************************************************

       subroutine coordinate_site
!coarse-graining 

         use variables,only:Nring,Npolymer,Nsite,Natom,x,y,z,x_site,&
              y_site,z_site
           
         implicit none
            
         integer ::py,j,i,n,site
       
         do py=1,Npolymer
            
            j=1
    
               do i=0,Nring-2
     
                  x_site(j,py)=(x(1+8*i,py)+x(4+8*i,py))/2.d0
                  y_site(j,py)=(y(1+8*i,py)+y(4+8*i,py))/2.d0 
                  z_site(j,py)=(z(1+8*i,py)+z(4+8*i,py))/2.d0
                  
                  x_site(j+1,py)=(x(7+8*i,py)+x(8+8*i,py))/2.d0
                  y_site(j+1,py)=(y(7+8*i,py)+y(8+8*i,py))/2.d0
                  z_site(j+1,py)=(z(7+8*i,py)+z(8+8*i,py))/2.d0

                  j=j+2

               end do
               
           x_site(Nsite,py)=(x(1+8*(Nring-1),py)+x(4+8*(Nring-1),py))/2.d0
           y_site(Nsite,py)=(y(1+8*(Nring-1),py)+y(4+8*(Nring-1),py))/2.d0
           z_site(Nsite,py)=(z(1+8*(Nring-1),py)+z(4+8*(Nring-1),py))/2.d0
               
         end do
       
       end subroutine coordinate_site

!***********************************************************************

         subroutine atom_allocated_to_site

           use variables,only:Natom,c_s
           
           implicit none

           integer :: a,k,unit_cell,cg
          
           do a=1,Natom

              k=mod(a,8)
              
              unit_cell=int((a-1)/8)+1
              
                  if(k.lt.7.and.k.gt.0) then
                     cg=(2*unit_cell)-1
                  else 
                     cg=2*unit_cell
                  end if

              c_s(a)=cg
  
           end do

         end subroutine atom_allocated_to_site

!***********************************************************************


     subroutine average_electron_position

       use variables

       implicit none

       integer :: py,leg,site,i,k

       CofM=0.d0

       do py=1,Npolymer
          do leg=1,nlegs(py)
             do site=1,Nsite

                CofM(1,leg,py)=CofM(1,leg,py)+(psi_legs(site,leg,py)**2)*&
                     x_site(site,py)
                CofM(2,leg,py)=CofM(2,leg,py)+(psi_legs(site,leg,py)**2)*&
                     y_site(site,py)
                CofM(3,leg,py)=CofM(3,leg,py)+(psi_legs(site,leg,py)**2)*&
                     z_site(site,py)

             end do
          end do
       end do

     end subroutine average_electron_position

!***********************************************************************

      
   subroutine leg_localised_length

     use variables,only:Nsite,Npolymer,nlegs,psi_legs,leg_loc,&
          Neigenstate,ave_leg_loc
         
     implicit none
     
     integer::py,leg,site,i
     double precision :: n2, n_mean,a

     leg_loc=0.d0
     
     do py=1,Npolymer
        
        do leg=1,nlegs(py)
           
           n_mean = 0.0d0
           n2 = 0.0d0
           do site=1,Nsite
                 
              n_mean = n_mean + site*psi_legs(site,leg,py)**2
              n2 = n2 + (site*psi_legs(site,leg,py))**2
              
           end do

           leg_loc(leg,py) = 5.5*dsqrt(n2 - n_mean**2)
           
        end do
        
     end do

!ave_leg_loc
     i=0.d0
     a=0.d0
     do py=1,Npolymer
        do leg=1,nlegs(py)
           i=i+1
           a = a + leg_loc(leg,py)
        end do
     end do

     ave_leg_loc=a/i
     
     end subroutine leg_localised_length
          
!*********************************************************************

     subroutine pz_carbon

       use variables,only:Nring,Natom,Neigenstate,Npolymer,psi_legs,&
            nlegs,c_s,prob_e_pz

       implicit none
       
       integer::i,a
       integer::py,leg,site
       double precision, dimension(Natom)::phi_c

       prob_e_pz=0.d0
       
       do i=0,Nring-1
          phi_c(1+8*i)=1.d0/dsqrt(3.d0)
       end do

       do i=0,Nring-1
          phi_c(2+8*i)=-1.d0/dsqrt(12.d0)
       end do

       do i=0,Nring-1
          phi_c(3+8*i)=-1.d0/dsqrt(12.d0)
       end do

       do i=0,Nring-1
          phi_c(4+8*i)=1.d0/dsqrt(3.d0)
       end do

       do i=0,Nring-1
          phi_c(5+8*i)=-1.d0/dsqrt(12.d0)
       end do

       do i=0,Nring-1
          phi_c(6+8*i)=-1.d0/dsqrt(12.d0)
       end do

       do i=0,Nring-2
          phi_c(7+8*i)=1.d0/dsqrt(2.d0)
       end do

       do i=0,Nring-2
          phi_c(8+8*i)=1.d0/dsqrt(2.d0)
       end do
       

       do py=1,Npolymer
          do leg=1,nlegs(py)
             do a=1,Natom
                site = c_s(a)
                prob_e_pz(a,leg,py)=psi_legs(site,leg,py)*phi_c(a)
             end do
           end do
        end do
        
     end subroutine pz_carbon

!**********************************************************************

          

   subroutine twin_polymer 

     use variables,only:Natom,Npolymer,x,y,z,n_cg,E_legs,psi_legs,nlegs,&
          CofM,prob_e_pz,Npolymer_t,x_t,y_t,z_t,n_cg_t,E_legs_t,&
          psi_legs_t,nlegs_t,CofM_t,prob_e_pz_t,bz,bxy,Nsite, &
          Neigenstate,flag_t,CofM_polymer,CofM_polymer_t,long
     
     implicit none

     integer::py,i,k,site,leg,a,j

     print*,bxy,bz

!coordinates of twin

      do py=1,Npolymer
      
         x_t(:,py+Npolymer) = x(:,py)
         y_t(:,py+Npolymer) = y(:,py)
         z_t(:,py+Npolymer) = z(:,py) +long(py)
         
         x_t(:,py)=x(:,py)
         y_t(:,py)=y(:,py)
         z_t(:,py)=z(:,py)

         x_t(:,py+2*Npolymer) = x(:,py)
         y_t(:,py+2*Npolymer) = y(:,py)
         z_t(:,py+2*Npolymer) = z(:,py) -long(py)
         
         x_t(:,py+3*Npolymer) = x(:,py)
         y_t(:,py+3*Npolymer) = y(:,py)+ bxy
         z_t(:,py+3*Npolymer) = z(:,py) 

         x_t(:,py+4*Npolymer) = x(:,py)
         y_t(:,py+4*Npolymer) = y(:,py)- bxy
         z_t(:,py+4*Npolymer) = z(:,py)

         x_t(:,py+5*Npolymer) = x(:,py) + bxy
         y_t(:,py+5*Npolymer) = y(:,py)
         z_t(:,py+5*Npolymer) = z(:,py)

         x_t(:,py+6*Npolymer) = x(:,py)- bxy
         y_t(:,py+6*Npolymer) = y(:,py)
         z_t(:,py+6*Npolymer) = z(:,py)


      end do   

! normal to coarse grain for each site of twin polymer

      do py=1,Npolymer
         do site=1,Nsite
            do k=1,3
               n_cg_t(k,site,py)=n_cg(k,site,py)
               n_cg_t(k,site,py+Npolymer)=n_cg(k,site,py)
               n_cg_t(k,site,py+2*Npolymer)=n_cg(k,site,py)
               n_cg_t(k,site,py+3*Npolymer)=n_cg(k,site,py)
               n_cg_t(k,site,py+4*Npolymer)=n_cg(k,site,py)
               n_cg_t(k,site,py+5*Npolymer)=n_cg(k,site,py)
               n_cg_t(k,site,py+6*Npolymer)=n_cg(k,site,py)

            end do
         end do
      end do
              
! legs of twin

      do py=1,Npolymer
         nlegs_t(py)=nlegs(py)
         nlegs_t(py+Npolymer)=nlegs(py)
         nlegs_t(py+2*Npolymer)=nlegs(py)
         nlegs_t(py+3*Npolymer)=nlegs(py)
         nlegs_t(py+4*Npolymer)=nlegs(py)
         nlegs_t(py+5*Npolymer)=nlegs(py)
         nlegs_t(py+6*Npolymer)=nlegs(py)
      end do

      psi_legs_t=0.d0
      E_legs_t=0.d0

      do py=1,Npolymer
         do leg=1,nlegs(py)
            do site=1,Nsite
               psi_legs_t(site,leg,py)=psi_legs(site,leg,py)
               psi_legs_t(site,leg,py+Npolymer)=psi_legs(site,leg,py)
               psi_legs_t(site,leg,py+2*Npolymer)=psi_legs(site,leg,py)
               psi_legs_t(site,leg,py+3*Npolymer)=psi_legs(site,leg,py)
               psi_legs_t(site,leg,py+4*Npolymer)=psi_legs(site,leg,py)
               psi_legs_t(site,leg,py+5*Npolymer)=psi_legs(site,leg,py)
               psi_legs_t(site,leg,py+6*Npolymer)=psi_legs(site,leg,py)

            end do

            E_legs_t(leg,py)=E_legs(leg,py)
            E_legs_t(leg,py+Npolymer)=E_legs(leg,py)
            E_legs_t(leg,py+2*Npolymer)=E_legs(leg,py)
            E_legs_t(leg,py+3*Npolymer)=E_legs(leg,py)
            E_legs_t(leg,py+4*Npolymer)=E_legs(leg,py)
            E_legs_t(leg,py+5*Npolymer)=E_legs(leg,py)
            E_legs_t(leg,py+6*Npolymer)=E_legs(leg,py)
            
         end do
      end do

 ! average electron position CoM of twin

      CofM_t=0.d0
      do py=1,Npolymer
         do leg=1,nlegs(py)
            
            CofM_t(1,leg,py)= CofM(1,leg,py)
            CofM_t(2,leg,py)= CofM(2,leg,py)
            CofM_t(3,leg,py)= CofM(3,leg,py)

            CofM_t(1,leg,py+Npolymer)= CofM(1,leg,py)
            CofM_t(2,leg,py+Npolymer)= CofM(2,leg,py)
            CofM_t(3,leg,py+Npolymer)= CofM(3,leg,py) + long(py)
            
            CofM_t(1,leg,py+2*Npolymer)= CofM(1,leg,py)
            CofM_t(2,leg,py+2*Npolymer)= CofM(2,leg,py)
            CofM_t(3,leg,py+2*Npolymer)= CofM(3,leg,py) - long(py)

            CofM_t(1,leg,py+3*Npolymer)= CofM(1,leg,py)
            CofM_t(2,leg,py+3*Npolymer)= CofM(2,leg,py) + bxy
            CofM_t(3,leg,py+3*Npolymer)= CofM(3,leg,py) 

            CofM_t(1,leg,py+4*Npolymer)= CofM(1,leg,py)
            CofM_t(2,leg,py+4*Npolymer)= CofM(2,leg,py) - bxy
            CofM_t(3,leg,py+4*Npolymer)= CofM(3,leg,py)

            CofM_t(1,leg,py+5*Npolymer)= CofM(1,leg,py) + bxy
            CofM_t(2,leg,py+5*Npolymer)= CofM(2,leg,py) 
            CofM_t(3,leg,py+5*Npolymer)= CofM(3,leg,py)

            CofM_t(1,leg,py+6*Npolymer)= CofM(1,leg,py) - bxy
            CofM_t(2,leg,py+6*Npolymer)= CofM(2,leg,py) 
            CofM_t(3,leg,py+6*Npolymer)= CofM(3,leg,py)

         end do
      end do
     
!polymer centre of mass


      CofM_polymer_t=0.d0
      do py=1,Npolymer

         CofM_polymer_t(1,py)= CofM_polymer(1,py)
         CofM_polymer_t(2,py)= CofM_polymer(2,py)
         CofM_polymer_t(3,py)= CofM_polymer(3,py)
         
         CofM_polymer_t(1,py+Npolymer)= CofM_polymer(1,py)
         CofM_polymer_t(2,py+Npolymer)= CofM_polymer(2,py)
         CofM_polymer_t(3,py+Npolymer)= CofM_polymer(3,py) + long(py)
         
         CofM_polymer_t(1,py+2*Npolymer)= CofM_polymer(1,py)
         CofM_polymer_t(2,py+2*Npolymer)= CofM_polymer(2,py)
         CofM_polymer_t(3,py+2*Npolymer)= CofM_polymer(3,py)- long(py)
         
         CofM_polymer_t(1,py+3*Npolymer)= CofM_polymer(1,py)
         CofM_polymer_t(2,py+3*Npolymer)=CofM_polymer(2,py) + bxy
         CofM_polymer_t(3,py+3*Npolymer)= CofM_polymer(3,py)
         
         CofM_polymer_t(1,py+4*Npolymer)= CofM_polymer(1,py)
         CofM_polymer_t(2,py+4*Npolymer)= CofM_polymer(2,py) - bxy
         CofM_polymer_t(3,py+4*Npolymer)= CofM_polymer(3,py)
         
         CofM_polymer_t(1,py+5*Npolymer)= CofM_polymer(1,py) + bxy
         CofM_polymer_t(2,py+5*Npolymer)= CofM_polymer(2,py)
         CofM_polymer_t(3,py+5*Npolymer)= CofM_polymer(3,py)
         
         CofM_polymer_t(1,py+6*Npolymer)= CofM_polymer(1,py) - bxy
         CofM_polymer_t(2,py+6*Npolymer)= CofM_polymer(2,py)
         CofM_polymer_t(3,py+6*Npolymer)= CofM_polymer(3,py)
         
      end do   

! prob amplitude of being on pz of each carbon of twin
      
      prob_e_pz_t=0.d0
      do py=1,Npolymer
         do leg=1,nlegs(py)
            do a=1,Natom
               prob_e_pz_t(a,leg,py)=prob_e_pz(a,leg,py)
               prob_e_pz_t(a,leg,py+Npolymer)=prob_e_pz(a,leg,py)
               prob_e_pz_t(a,leg,py+2*Npolymer)=prob_e_pz(a,leg,py)
               prob_e_pz_t(a,leg,py+3*Npolymer)=prob_e_pz(a,leg,py)
               prob_e_pz_t(a,leg,py+4*Npolymer)=prob_e_pz(a,leg,py)
               prob_e_pz_t(a,leg,py+5*Npolymer)=prob_e_pz(a,leg,py)
               prob_e_pz_t(a,leg,py+6*Npolymer)=prob_e_pz(a,leg,py)
            end do
         end do
      end do

      open(unit=26,file='coordinates3.xyz',status='unknown')
      write(26,*)Natom*Npolymer_t
      write(26,*)
      do py=1,Npolymer_t
         do j = 1,Natom
            write(26,102)py, x_t(j,py),y_t(j,py),z_t(j,py)
102         format(i4,e14.5,e14.5,e14.5)
         end do
      end do
      close(26)

      open(unit=35,file='cofm.txt',status='unknown')
      write(35,*)Npolymer_t-1
      write(35,*)
      do py=1,Npolymer_t
         write(35,102)1,CofM_polymer_t(1,py),CofM_polymer_t(2,py), &
              & CofM_polymer_t(3,py)
      end do
      close(35)
      
    end subroutine twin_polymer


!**********************************************************************

    subroutine overlap

      use variables,only:Npolymer,Npolymer_t,Nsite,Natom,Neigenstate, &
           n_cg_t,pi,x_t,y_t,z_t,c_s,prob_e_pz_t,nlegs_t,W_da, &
           CofM_polymer_t,ave_rad_gyr,n_map,ny,CofM_t
    
    implicit none

    integer::py,i,j,qy,a,d,b
    integer::site_i,site_j
    double precision::bluh,p,dot,temp1,sep,temp5
    double precision::phi,omega,theta,S,S_r,W_ij
    double precision::toverlap,tpy
    double precision,dimension(3)::r, temp2, temp3, temp4
    double precision::xi,yi,zi
    double precision::total_wda,ave_wda
    
       sep=0.d0
       p=3.07d0

       W_da=0.0d0

       n_map=0.d0
       ny=0.d0

       print*,'rg',ave_rad_gyr

       do py=1,Npolymer
          do d=1,nlegs_t(py)
             i=0.d0
             do qy=1,Npolymer_t
                do a=1,nlegs_t(qy)
                   xi=CofM_t(1,d,py)-CofM_t(1,a,qy)
                   yi=CofM_t(2,d,py)-CofM_t(2,a,qy)
                   zi=CofM_t(3,d,py)-CofM_t(3,a,qy)
                   sep=dsqrt(xi**2+yi**2+zi**2)
                   
                   if ((sep.le.50.d0).and.(py.ne.qy)) then
                      
                      ny(py,d)=ny(py,d)+1
                      i=i+1
                      n_map(py,d,i,1)=qy
                      n_map(py,d,i,2)=a
            
                   end if
                   

                end do
             end do
               
          end do
       end do

!magnitude of normal vector
!separation between i and j

       do py=1,Npolymer
          print*,py
          do d=1,nlegs_t(py)
             do b=1,ny(py,d)
                qy=n_map(py,d,b,1)
                a=n_map(py,d,b,2)
                                      
                   do i=1,Natom
                      do j=1,Natom
                       
                         omega=0.d0
                         theta=0.d0
                         phi=0.d0
                         S=0.d0
                         S_r=0.d0
                         r=0.d0
                         W_ij=0.d0
                         
                         xi=x_t(i,py)-x_t(j,qy)
                         yi=y_t(i,py)-y_t(j,qy)
                         zi=z_t(i,py)-z_t(j,qy)
                         sep=dsqrt(xi**2+yi**2+zi**2)
                           
                         if (sep.le.2.5d0) then
                            r=0.d0
                            S_r=0.d0
                         else
                            r(1)=(xi)/sep
                            r(2)=(yi)/sep
                            r(3)=(zi)/sep
                            
                            S_r=dexp(-p*sep)*(1.0d0+p*sep+ &
                                 & 0.4d0*(p*sep)**2+(p*sep)**3/15.0d0)
                            
                         end if
                         
                         site_i=c_s(i)
                         site_j=c_s(j)
                         
                         temp2(:) = n_cg_t(:,site_i,py)
                         temp3 = r
                         omega=pi/2-dacos(dot(temp2,temp3))
                         
                         
                         temp4(:)=n_cg_t(:,site_j,qy)
                         
                         theta=-(pi/2-dacos(dot(temp4,-temp3)))
                         
                         phi=dot(temp2,temp4)
                         
!overlap integral between atomic sites a and b
                         
                         temp1 = phi*dcos(omega)*dcos(theta)
                         
                         S=temp1*S_r
                            
!resonance integral between atomic sites
                   
                         W_ij=10.6d0*S ! in eV
                                                  
                        W_da(d,a,py,qy)=W_da(d,a,py,qy)+&
                             &W_ij*prob_e_pz_t(i,d,py)*prob_e_pz_t(j,a,qy)
                         
                      end do
                   end do
             end do
           end do
        end do
          
   !AVERAGE OVERLAP INTERGRAL SQUARED

       j=0
       total_wda=0.d0
       do py=1,Npolymer
          do d=1,nlegs_t(py)
             do i=1,ny(py,d)
                qy=n_map(py,d,i,1)
                a=n_map(py,d,i,2)
              
                j=j+1

                total_wda=total_wda+(W_da(d,a,py,qy)**2)

             end do
          end do
       end do

       print*,'total_wda',total_wda
       print*,'number w_da',j

       ave_wda=total_wda/dble(j)

       open(unit=12,file='overlap',status='unknown')
           write(12,299)ave_wda
299        format(e15.9)
       close(12)


   end subroutine overlap

!***********************************************************************

   subroutine marcus_charge_transfer
        
     use variables,only:Npolymer,Nsite,Natom,Neigenstate,Npolymer_t, &
          psi_legs_t,E_legs_t,nlegs_t,W_da,Ninterval,hbar,kb,T,pi, &
          E_lambda,CofM_t,field,k_da,n_map,ny

     implicit none

     double precision::E0,En,h,F,A_E,eQ
     integer::i,d,a,py,qy,c,l,leg
     
     double precision::trap
     double precision::temp
     double precision, dimension(Nsite)::s_c
     double precision,dimension(Nsite,Neigenstate,Npolymer_t)::tp
     double precision,dimension(Neigenstate,Npolymer_t)::s_eff
     double precision :: E_d, E_a, E

     integer::trap_no
     double precision::trap_total,trap_ave,k_total,k_ave

     eQ=1.60217656535d-19
  
     tp=0.d0
     s_eff=0.d0
     E_lambda=0.d0

     do i=1,Nsite,2
        s_c(i)=0.91d0
     end do

     do i=2,Nsite-1,2
        s_c(i)=2.d0
     end do

     do py=1,Npolymer_t
        do i=1,nlegs_t(py)
           do c=1,Nsite
              tp(c,i,py)=s_c(c)*psi_legs_t(c,i,py)**4        
           end do
           s_eff(i,py)=sum(tp(:,i,py))
           E_lambda(i,py)=0.2d0*s_eff(i,py)
        end do
     end do

     do py=1,Npolymer_t
        do leg=1,nlegs_t(py)
           E_legs_t(leg,py)=E_legs_t(leg,py)-field*CofM_t(1,leg,py) ! x=1 2=y
!z=3
        end do
     end do

     open(unit=70,file="LEG_energy_new",status='unknown')
     do py=1,Npolymer
        do leg=1,nlegs_t(py)
           write(70,*)E_legs_t(leg,py)
        end do
        write(70,*)
     end do
     close(70)

     E0 = -2.0 !eV
     En = 6.0 !eV
     h=(En-E0)/dble(Ninterval)
     k_da=0.d0

     trap_total=0.d0
     k_total = 0.0d0
     trap_no=0

     do py=1,Npolymer
        do d=1,nlegs_t(py)
           do i=1,ny(py,d)
              qy=n_map(py,d,i,1)
              a=n_map(py,d,i,2)                 

                 trap=0.d0
                 temp=0.d0

                 do l=0,Ninterval
                    E= E0 + h*dble(l)
                    E_d = (E_legs_t(d,py) - E)
                    E_a = (E - E_legs_t(a,qy))
                    temp=temp+ F(pi,kb,T,E_d,E_a,E_lambda(d,py),&
                         E_lambda(a,qy))

                 end do

!first interval

                 E_d = (E_legs_t(d,py) - E0)
                 E_a = (E0 - E_legs_t(a,qy))

                 temp = temp-0.5d0*F(pi,kb,T,E_d,E_a,E_lambda(d,py),&
                      E_lambda(a,qy))

!final interval

                 E_d = (E_legs_t(d,py) - En)
                 E_a = (En - E_legs_t(a,qy))

                 temp = temp-0.5d0*F(pi,kb,T,E_d,E_a,E_lambda(d,py),&
                      E_lambda(a,qy))

                 trap = temp * h

                 trap_total=trap_total+trap
                 trap_no=trap_no+1                
!k_da, s**-1
                 k_da(d,a,py,qy)=(2.d0*pi*eQ/hbar)*(W_da(d,a,py,qy)**2)*&
                      trap
   
                 k_total = k_total + k_da(d,a,py,qy)

                 k_da(d,d,py,py)=0.d0
              
            end do
         end do
      end do

      print*,'trap_total',trap_total
      print*,'trap_number',trap_no

      trap_ave=trap_total/dble(trap_no)      
      k_ave = k_total/dble(trap_no)

      open(unit=33,file='trap',status='unknown')
          write(33,299)trap_ave,k_ave
299       format(2e15.9)
      close(33)

      open(unit=19,file='rates',status='unknown')

     do py=1,Npolymer
        do d=1,nlegs_t(py)
           do i=1,ny(py,d)
              qy=n_map(py,d,i,1)
              a=n_map(py,d,i,2)

              write(19,300)k_da(d,a,py,qy),py,qy,d,a
           
           end do
        end do
     end do
     close(19)

300   format(e13.5,4i4)

101  format('k_DA',i3,1x,i3,1x,i3,1x,i3,1x,4e13.5)

    

    end subroutine marcus_charge_transfer

!**********************************************************************

!this is the monte carlo where for each possible initial don\or the acceptor is chosen and the time after transition is added
!working backwards we then work out for every possible init\ial donor
    
     subroutine monte_carlo

       use variables,only:Npolymer,Npolymer_t,nlegs,nlegs_t,k_da,&
            time,hop_number,Neigenstate,CofM_t,flag_t,bz,bxy,iseed,&
            distance,rmsdistance,drift_velocity,field,mobility,n_map,ny,&
            CofM_polymer_t

       implicit none

       integer::ic
       integer::d,donor0,donor_state0,donor,donor_state,a,qy,i,leg
       integer::acceptor,acceptor_state,py,f
       integer::dummy
       integer::hmax,h,j,ha

       double precision::delta_y,delta_x,r
       double precision::x,ran1,delta_t,z0,delta_z,drift
       double precision,dimension(Npolymer,Neigenstate)::rms_gradient,&
            t_d_error,t_rmsd_error,intercept
       double precision::dt,dt_0

       double precision,dimension(0:5*Npolymer_t,Npolymer,Neigenstate)::&
            k_total,k_total_ave
       double precision::ave_mobility,ave_drift
       integer, dimension(2,5*Npolymer_t,Npolymer,Neigenstate)::rate_map

       integer::no
       double precision::ktotal_sum,ktotal_average
       integer, dimension(100)::N
       integer:: np,mp,num
       integer,dimension(-100:100)::M

       distance=0.d0
       time=0.d0
       rmsdistance=0.d0
       mobility=0.d0
       hop_number=0.d0

       k_total=0.d0
       k_total_ave=0.d0
       rate_map=0.d0
       ktotal_sum=0.d0
       no=0 
       N=0  
       M=0
       num=0
! calculating cumulative rates and forming rate map

       do donor=1,Npolymer
          do donor_state=1,nlegs(donor)

             h=0
             k_total_ave(0,donor,donor_state)=0.d0
             k_total(0,donor,donor_state)=0.d0
                             
             do i=1,ny(donor,donor_state)
                qy=n_map(donor,donor_state,i,1)
                a=n_map(donor,donor_state,i,2)
                
                if (qy.eq.0.or.a.eq.0) then
                   print*,donor0,donor_state0,donor,donor_state
                   stop
                end if
                
                h=h+1
                
                k_total(h,donor,donor_state)=k_total&
                     (h-1,donor,donor_state)+k_da(donor_state,a,donor,qy)

                ktotal_sum=ktotal_sum+k_da(donor_state,a,donor,qy)
                no=no+1
      
               if (k_total(h,donor,donor_state).eq.0)then
                  print*,'nooooo'
               end if
            
                rate_map(1,h,donor,donor_state)=qy
                rate_map(2,h,donor,donor_state)=a

             end do
                                          
             k_total_ave(:,donor,donor_state)=k_total(:,donor,donor_state&
                  )/k_total(ny(donor,donor_state),donor,donor_state)

         end do
      end do


      print*,'ktotal_sum',ktotal_sum
      print*,'number ktotal',no

      ktotal_average=ktotal_sum/dble(no)

      open(unit=67,file='ktotal',status='unknown')
           write(67,299)ktotal_average
      close(67)

!MONTE CARLO
      

     open(unit=50,file='distance',status='unknown')
     open(unit=51,file='time',status='unknown') 

      do donor0=1,Npolymer !for different initial donors-in the box
         do donor_state0=1,nlegs_t(donor0)

             donor=donor0
             donor_state=donor_state0           
            
             ic=2

             distance(1)=0.d0
             time(1)=0.d0
             rmsdistance(1)=0.d0
             
             do while(time(ic).lt.1d-5)
                
                ic=ic+1
                
                if (ic.eq.1000001)then
                   exit
                end if
                              
                x=ran1(iseed)
                
                do h=1,ny(donor,donor_state)
                   ha=h
                   if (k_total_ave(h,donor,donor_state).ge.x) exit
                end do

                acceptor=rate_map(1,ha,donor,donor_state)
                acceptor_state=rate_map(2,ha,donor,donor_state)

                if (acceptor.eq.0.or.acceptor_state.eq.0) then
                   print*,'2',donor0,donor_state0,donor,donor_state
                   stop
                end if
                
!DWELL TIME

                x=ran1(iseed)
                dt=-dlog(x)/k_total(ny(donor,donor_state),donor,&
                     donor_state)             
      

                time(ic)=time(ic-1)+dt
                
                delta_z=CofM_t(1,acceptor_state,acceptor)-CofM_t&
                     (1,donor_state,donor)
                
                delta_x=CofM_t(3,acceptor_state,acceptor)-CofM_t&
                     (3,donor_state,donor)

                delta_y=CofM_t(2,acceptor_state,acceptor)-CofM_t&
                     (2,donor_state,donor)                

                distance(ic)=distance(ic-1)+delta_z
                
                rmsdistance(ic)=rmsdistance(ic-1)+delta_z**2

               
                if (acceptor.gt.5*Npolymer.and.acceptor.le.6*Npolymer*2)&
                     acceptor=acceptor-5*Npolymer
                
                if (acceptor.gt.6*Npolymer.and.acceptor.le.Npolymer*7)&
                     acceptor=acceptor-6*Npolymer

                
                if (acceptor.gt.3*Npolymer.and.acceptor.le.4*Npolymer)&
                     acceptor=acceptor-3*Npolymer


                if (acceptor.gt.4*Npolymer.and.acceptor.le.Npolymer*5) &
                     acceptor=acceptor-4*Npolymer

                
                if (acceptor.gt.Npolymer.and.acceptor.le.Npolymer*2)&
                     acceptor=acceptor-Npolymer
              
                
                if (acceptor.gt.2*Npolymer.and.acceptor.le.Npolymer*3)&
                     acceptor=acceptor-2*Npolymer

                donor=acceptor
                donor_state=acceptor_state


                if(donor.gt.Npolymer.or.donor.lt.0) then
                   print*,'3',donor0,donor_state0,donor,donor_state
                   stop
                end if

                r=dsqrt(delta_z**2+delta_x**2+delta_y**2)

                np=int(r/5)+1
                N(np)=N(np)+1

                mp=int(delta_z/5)+1
                M(mp)=M(mp)+1

                num=num+1

             end do
             
             call least_squares_fit(time,distance,drift,intercept,&
                  t_d_error)
             call least_squares_fit(time,rmsdistance,rms_gradient,&
                  intercept,t_rmsd_error)

             drift_velocity(donor0,donor_state0)=drift

             if (maxval(distance).gt.bxy) then
                print*,'box'
             end if

             write(50,*)donor0,donor_state0,maxval(distance)
             write(51,*)donor0,donor_state0,maxval(time)

          end do
       end do

       print*,'number of hops, num',num
          
       close(50)
       close(51)

       open(unit=77,file='hop_distance',status='unknown')
           do np=1,100
              write(77,*)np,5*np,N(np)
           end do
       close(77)

       open(unit=78,file='hop_field_distance',status='unknown')
           do mp=-100,100
              write(78,*)mp,5*mp,M(mp)
           end do
       close(77)

       open(unit=15,file='drift_velocity',status='unknown')

       do py=1,Npolymer
          do leg=1,nlegs_t(py)
             write(15,*)drift_velocity(py,leg)
          end do
       end do

       close(15)

       if (field.gt.0.d0) then
          open(unit=17,file='mobility',status='unknown')
          do py=1,Npolymer
             do leg=1,nlegs_t(py)
                mobility(py,leg)=drift_velocity(py,leg)/field
                write(17,299)mobility(py,leg)
299             format(e15.9)
             end do
          end do
          close(17)
       end if

       ave_drift=0.d0
       i=0.d0
       do py=1,Npolymer
          do leg=1,nlegs(py)
             ave_drift=ave_drift+drift_velocity(py,leg)
             i=i+1
          end do
       end do
       ave_drift=ave_drift/i

       ave_mobility=0.d0
       i=0.d0
       do py=1,Npolymer
          do leg=1,nlegs(py)
             ave_mobility=ave_mobility+mobility(py,leg)
             i=i+1
          end do
      end do
      ave_mobility=ave_mobility/i

      open(unit=50,file='average_mobility',status='unknown')!status='old',access='append')
      write(50,299)ave_drift
      write(50,299)ave_mobility
      close(50)

     end subroutine monte_carlo

!*********************************************************************

  subroutine least_squares_fit(x,y,m,c,ei2)

    use variables,only:Npolymer,Neigenstate,nlegs

    implicit none
    
    double precision:: x(1000000),y(1000000), xy(1000000), xx(1000000),&
         ey(1000000),ey2(1000000)
    double precision::m,ei2,c
    double precision::avex,avey,d
    
    c=0.d0
    m=0.d0
    ei2=0.d0
    xy=0.d0
    xx=0.d0
    ey=0.d0
    ey2=0.d0
    avex=0.d0
    avey=0.d0

    d=1000000.d0
    avex=sum(x(:))/d
    avey=sum(y(:))/d
    xy(:)=x(:)*y(:)
    xx(:)=x(:)*x(:)
    
    m=((sum(x(:))*sum(y(:)))-(d*sum(xy)))/((sum(x(:)))**2-d*sum(xx))
    
    c=(sum(y(:))-m*sum(x(:)))/d
    ey(:)=y(:)-(m*x(:)+c)
    ey2=ey*ey
    ei2=sum(ey2)/d

  end subroutine least_squares_fit

    

!**********************************************************************

    function X_E(pi,kb,T,E,E_lambda)

      implicit none

      double precision::kb,pi,T,E,X_E,E_lambda

      X_E = dsqrt(1.0d0/(4.0d0*pi*kb*T*E_lambda))
      X_E = X_E*dexp(-(E-E_lambda)**2/(4.0d0*kb*T*E_lambda))

    end function X_E
    
!*********************************************************************

    function F(pi,kb,T,E_d,E_a,E_lambda_d,E_lambda_a)

      implicit none

      double precision::pi,kb,T
      double precision::E_d,E_a,F,X_E
      double precision::E_lambda_a,E_lambda_d

       F=X_E(pi,kb,T,E_d,E_lambda_d)*X_E(pi,kb,T,E_a,E_lambda_a)

     end function F

!***********************************************************************  

      function random(iseed)

       implicit none
       real*8 :: random
       integer :: iseed, ia, ic, im

       im = 714025
       ia = 4096
       ic = 150889
       iseed = mod( iseed * ia + ic, im )
       random = dble(iseed) / dble(im)
       random = (random + 1.0d0)/2.0d0

       end function random

       FUNCTION ran1(iseed)


       INTEGER :: iseed,IA,IM,IQ,IR,NTAB,NDIV
       REAL*8 :: ran1,AM,EPS,RNMX
       PARAMETER (IA=16807,IM=2147483647,AM=1.0d0/IM,IQ=127773,IR=2836, &
       & NTAB=32,NDIV=1+(IM-1)/NTAB,EPS=1.2d-7,RNMX=1.-EPS)
       INTEGER :: j,k,iv(NTAB),iy
       SAVE iv,iy
       DATA iv /NTAB*0/, iy /0/

       if (iseed.le.0.or.iy.eq.0) then
         iseed=max(-iseed,1)
         do 11 j=NTAB+8,1,-1
           k=iseed/IQ
           iseed=IA*(iseed-k*IQ)-IR*k
           if (iseed.lt.0) iseed=iseed+IM
           if (j.le.NTAB) iv(j)=iseed
11       continue
         iy=iv(1)
       endif
       k=iseed/IQ
       iseed=IA*(iseed-k*IQ)-IR*k
       if (iseed.lt.0) iseed=iseed+IM
       j=1+iy/NDIV
       iy=iv(j)
       iv(j)=iseed
       ran1=min(AM*iy,RNMX)
       return
       END function ran1

       FUNCTION gasdev(iseed,sig,mean_g)

!      page 280, Numerical Recipes
!      Returns a Gaussian distributed random variable of
!      "mean" value and standard deviation "sig".

       INTEGER :: iseed
       REAL*8 :: gasdev, sig, mean_g
       INTEGER :: iset
       REAL*8 :: fac,gset,rsq,v1,v2,ran1,random
       SAVE iset,gset
       DATA iset/0/
       if (iseed.lt.0) iset=0
       if (iset.eq.0) then
1        v1=2.0d0*ran1(iseed)-1.0d0
         v2=2.0d0*ran1(iseed)-1.0d0
!1        v1=2.0d0*random(iseed)-1.0d0
!         v2=2.0d0*random(iseed)-1.0d0
         rsq=v1**2+v2**2
         if (rsq.ge.1.0d0.or.rsq.eq.0.0d0) goto 1
         fac=dsqrt(-2.0d0*dlog(rsq)/rsq)
         gset=v1*fac
         gasdev=v2*fac
         iset=1
       else
         gasdev=gset
         iset=0
       endif
       gasdev = sig * gasdev + mean_g
       return
       END function gasdev


