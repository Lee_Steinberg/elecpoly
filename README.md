# Electron transfer in polymer chains
### Lee Steinberg (Initial code written by Alice Grout-Smith)

## File List
`program.f90` - The main Fortran90 code

`stable.f90` - The most recent stable version of the code

`parameters` - The file containing the parameters for a calculati0on

`edit_history` - An easy way to see the edits made to the code in turn. This does not contain the information about the edits to readme etc.

`README.md` - This README file

`testing` - A directory for testing of the code

`to_do.md` - A to-do list for Lee

## How to read the code
Lee's edits are labelled by the lines `!!!n`, where n corresponds to the edit number in edit_history. 

## How to complile the code
The code uses the pgf90 compiler. In order to use the compiler (on Dirac) must run `module load pgi/2012`. Have not compliled on Coulson etc., but should be available.

Code is then compiled with the command `pgf90 program.f90 --llapack -lblas -fastsse -o #NAME#.exe`

## How to run the code
Again, these instructions are Dirac specific (but can be modified to fit your system). A qscript must be generated, and then accordingly sent off. The template qscript is in `genQ.sub`. The file `send_job.sh` is a simple bash script that accordingly edits the template and sends the job to the queue. `send_job.sh` can be executed from any directory, provided it has the right information about where to find `genQ.sub`, and the `.exe` and `parameters` file are present in the directory you wish to send data to.
