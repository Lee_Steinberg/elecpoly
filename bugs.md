# Known Bugs (and potential fixes)

### Compiler Errors

- Code does not compile with gfortran. There are two causes of this error.
-  The first is the definition of cross - it seems that cross was originally due to be defined as a function, but instead was used as a subroutine. This should have a relatively simple fix, of removing the places were cross is declared with a type in the variables module
- The second is the `least_squares_fit` subroutine, and is due to an error in the type of variable that the function is expecting for its third and fourth argument when it is called. This can be fixed by editing the subroutine itself
