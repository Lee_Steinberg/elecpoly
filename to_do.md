# TO DO 
### Lee Steinberg

- [x] Add Nelec variable, ensure it can be read in etc.
- [ ] Understand difference between QEES and LEGS
- [ ] Understand nature of Lifshitz tail and van Hove singularities
- [x] Determine way to find useful orbital set, rather than LEGSs. 
- [ ] Find way to define a delta when determining orbital sets
- [ ] Determine way of filling states
- [x] Understand average electron number for determining orbital set
