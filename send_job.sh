#!/bin/bash


lib=`pwd`
file="program.exe"
sed -e s\0SET_DIR\0$lib\0 ~/summer/code_edits/genQ.sub > qscript.sub #\0 character to separate the sed command. cant use slashes because pwd has slashes in it.
sed -e s/EXECUTABLE/$file/ qscript.sub > qscript_2.sub
mv qscript_2.sub qscript.sub
echo "JobScript Generated"
qsub qscript.sub
echo "Job sent"
qstat -u quee3242
