| Edit Number | Date | Time | Description |
| ----------- | ---- | ---- | ----------- |
| 0 | 04/07/16 | 11:00 | Initial edit to code, add author information etc. |
| 1 | 04/07/16 | 11:58 | Create LEE_OUTPUT file, add Nelec variable |
| 2 | 06/07/16 | 11:39 | Create find_orbital subroutine |
| 2 | 06/07/16 | 11:46 | Check calculates average number of orbitals correctly |
| 3 | 06/07/16 | 13:58 | Create method to calculate number of electrons in a polymer |
|3.1| 07/07/16 | 10:15 | See if program still works when using ave_Nelec to judge orbitals to use rather than LEGSs
|1.1| 07/07/16 | 11:49 | Edit nature of Nelec to doping fraction |
|3.2| 07/07/16 | 11:50 | Revert back to previous version of stable, where  find_orbitals was essentially a blank function. This is due to a flaw in how we were determining which orbitals to use
| 2 | 11/07/16 | 10:08 | Change type of ave_Nelec to integer
| 3 | 11/07/16 | 11:00 | Fix find_orbitals subroutine. Seems to work.
| 4 | 11/07/16 | 12:15 | Add new fill_orbitals subroutine, which determines initial occupation numbers of the system
| 5 | 13/07/16 | 10:30 | Add temperature dependence to find_orbitals
|5.1| 13/07/16 | 11:20 | Edit fill_orbitals to take into account varying number of orbitals on each polymer
| 6 | 14/07/16 | 10:34 | Edit Marcus_charge_transfer so that you cannot transfer to a filled orbital
| 6 | 14/07/16 | 10:42 | Realise marcus_charge_transfer might need a total overhaul, cry a little.
| 7 | 14/07/16 | 16:14 | Add a new monte_carlo routine. Still need to actually do the monte-carlo stuff, but now you select the right states to swap
| 8 | 15/07/16 | 09:30 | Realise you forgot to take into account PBCs. Kind of back to the drawing board. But now the original monte-carlo code looks a lot better!
